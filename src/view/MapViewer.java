package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

import model.Pair;
import model.Patch;
import model.State;
import utilities.Constants;


public class MapViewer extends JFrame {

	private JLayeredPane layer = new JLayeredPane();
	private VisualiserPanel mainPanel;
	private JPanel grid = new JPanel();
	private List<Patch> patchList;
	
	public MapViewer (List<Patch> pList) {
		this.patchList = pList;
		
		setSize((int)Constants.MapSize + 50, (int)Constants.MapSize + 50);
		setTitle("Map Monitor");
		Border border = BorderFactory.createLineBorder(Color.black);
		
		grid = new JPanel(new GridLayout(3,3));
		
		mainPanel = new VisualiserPanel(layer);
		mainPanel.setSize(new Dimension(600, 600));
		mainPanel.setBackground(Constants.MapColor);
		
		grid.setSize(new Dimension(600, 600));
		for (Patch p : patchList) {
			JPanel square = new JPanel(new BorderLayout()); 
			JButton button = new JButton("Id: " + p.getId());
			button.setBounds(p.getQuadrant().getTopLeftCorner().getComponentX().intValue(), p.getQuadrant().getTopLeftCorner().getComponentY().intValue(), 100, 20);
		//	square.add(button, BorderLayout.PAGE_START);
			this.getContentPane().add(button);
			button.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent ev) {
					p.alertResolved();
					System.out.println("Alert resolved in " + p.getId());
				}
			});
			square.setBorder(border);
			square.setOpaque(false);
			grid.add(square);
		}

		grid.setBorder(border);
		grid.setOpaque(false);
		
		
		layer.add(mainPanel);
		layer.add(grid);
		this.getContentPane().add(this.layer);
		
	    grid.setVisible(true);
		setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}

			public void windowClosed(WindowEvent ev) {
				System.exit(-1);
			}
		});
		
	}
	
	public void notifyAlert(Pair<Integer, Integer> id) {
		for (Patch p : this.patchList) {
			if (p.getId() == id) {
				// metterci l'azione che deve fare la view quando un patch va in allerta
				System.out.println("Patch " + p.getId() + " in allerta");
			}
		}
	}
	
	public void drawSensors(Map<Integer, Pair<Double, Double>> positions) {
		if (positions.size() != 0) {
			this.mainPanel.updateSensors(positions.entrySet().stream().map(x -> x.getValue()).collect(Collectors.toList()));
		}
	}
	
	public void drawGuardians(Map<Integer, Pair<Pair<Double, Double>, State>> guardians) {
		if (guardians.size() != 0) {
			this.mainPanel.updateGuardians(guardians.entrySet().stream().map(x -> x.getValue()).collect(Collectors.toList()));
		}
	}
	
	public static class VisualiserPanel extends JPanel {
        
		private List<Pair<Double, Double>> sensors;
        private List<Pair<Pair<Double, Double>, State>> guardians;
        private JLayeredPane layer;
        
        public VisualiserPanel(JLayeredPane layer){
        	this.setPreferredSize(new Dimension((int)Constants.MapSize, (int)Constants.MapSize));
        	this.sensors = new ArrayList<>();
        	this.guardians = new ArrayList<>();
        	this.layer = layer;
        }

        public void paint(Graphics g) {
    		Graphics2D g2 = (Graphics2D) g;
    		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
    		          RenderingHints.VALUE_ANTIALIAS_ON);
    		g2.setRenderingHint(RenderingHints.KEY_RENDERING,
    		          RenderingHints.VALUE_RENDER_QUALITY);
    		g2.setColor(getBackground());
    		g2.fillRect(0, 0, getWidth(), getHeight());
    		
            synchronized (this){
            	
	            if (this.sensors != null){
	                sensors.forEach( e -> {
	                	Integer x0 = e.getComponentX().intValue();
	                	Integer y0 = e.getComponentY().intValue();
	                	g2.setColor(Constants.SensorColor);
		                g2.drawOval(x0,y0, Constants.SensorSize,Constants.SensorSize);
		                g.fillOval(x0, y0, Constants.SensorSize, Constants.SensorSize);
		            });
	            }	 
	            
	            if (this.guardians != null) {
	            	this.guardians.forEach(e -> {
	            		Integer x0 = e.getComponentX().getComponentX().intValue();
	                	Integer y0 = e.getComponentX().getComponentY().intValue();
		                g2.drawOval(x0 , y0 , Constants.GuardianSize , Constants.GuardianSize);
		                if (e.getComponentY() == State.QUIET) {
		                	g2.setColor(Constants.QuietColor);
		                } else {
		                	g2.setColor(Constants.PreAlertColor);
		                }
		                g2.drawOval(x0,y0, Constants.GuardianSize,Constants.GuardianSize);
		                g.fillOval(x0, y0, Constants.GuardianSize, Constants.GuardianSize);
	            	});
	            }
            }
            this.layer.moveToBack(this);
            
        }
        
        public void updateSensors(List<Pair<Double, Double>> sens){
            synchronized(this){
                sensors = sens;
            }
            repaint();
        }
        
        public void updateGuardians(List<Pair<Pair<Double, Double>, State>> guardians) {
        	synchronized(this) {
        		this.guardians = guardians;
        	}
        	repaint();
        }
    }

}
