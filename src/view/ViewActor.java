package view;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import akka.actor.AbstractActor;
import messages.GuardianStateMsg;
import messages.GuardiansReferenceMsg;
import messages.SensorPositionMsg;
import messages.ValueNotificationMsg;
import model.Pair;
import model.PairImpl;
import model.State;
import utilities.ActorPosition;
import utilities.Constants;

public class ViewActor extends AbstractActor {
	
	private Map<Integer, Pair<Double, Double>> sensors;
	private Map<Integer, Pair<Pair<Double, Double>, State>> guardians;
	private ViewAgent viewUpdater;
	private Lock mutex;
	
	public ViewActor(MapViewer m) {
		this.mutex = new ReentrantLock();
		this.sensors = new HashMap<>();
		this.guardians = new HashMap<>();
		this.viewUpdater = new ViewAgent(m); 
		this.viewUpdater.start();
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder().match(GuardianStateMsg.class, msg -> {
			int id = msg.getActorPosition().getId();
			Pair<Double, Double> pos = msg.getActorPosition().getPosition();
			State s = msg.getState();
			this.mutex.lock();
			if (this.guardians.containsKey(id)) {
				this.guardians.replace(id, new PairImpl<Pair<Double,Double>, State>(pos, s));
			} else {
				this.guardians.put(id, new PairImpl<Pair<Double,Double>, State>(pos, s));
			}
			
			this.mutex.unlock();	
			
		}).match(SensorPositionMsg.class, msg -> {
			int id = msg.getActorPosition().getId();
			Pair<Double, Double> pos = msg.getActorPosition().getPosition();
			this.mutex.lock();
			if (this.sensors.containsKey(id)) {
				this.sensors.replace(id, pos);
			} else {
				this.sensors.put(id, pos);
			}
			
			this.mutex.unlock();
			
		}).build();
	}
	
	public class ViewAgent extends Thread {
		
		private MapViewer viewer;
		private Map<Integer, Pair<Double, Double>> sensors;
		private Map<Integer, Pair<Pair<Double, Double>, model.State>> guardians;
		
		public ViewAgent(MapViewer viewer) {
			this.viewer = viewer;
			this.sensors = new HashMap<Integer, Pair<Double,Double>>();
			this.guardians = new HashMap<Integer, Pair<Pair<Double,Double>, model.State>>();
		}
		
		public void run() {
			while(true) {
				try {
					Thread.sleep(Constants.viewRefreshTime);
					ViewActor.this.mutex.lock();
					this.sensors = ViewActor.this.sensors;
					this.guardians = ViewActor.this.guardians;
					ViewActor.this.mutex.unlock();	
					this.viewer.drawSensors(this.sensors);
					this.viewer.drawGuardians(this.guardians);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
}
