package model;

import java.util.Random;

import utilities.Constants;

public class RandomGenerator {

	private Random random = new Random();
	private static RandomGenerator instance;
	
	private RandomGenerator() {}
	
	public Double getRandomDoubleValue() {
		return random.nextDouble();
	}
	
	public Pair<Double, Double> getGuardianPosition(Pair<Integer, Integer> patchId) {
		double x = this.random.nextInt((int) (Constants.patchSize - 2*Constants.GuardianSize)) + Constants.GuardianSize;
		double y = this.random.nextInt((int) (Constants.patchSize - 2*Constants.GuardianSize)) + Constants.GuardianSize;
		
		return new PairImpl<Double, Double>(x + (patchId.getComponentX() * Constants.patchSize), y + (patchId.getComponentY() * Constants.patchSize));
	}
	
	public Pair<Double, Double> getNewRandomPosition(){
		return new PairImpl<Double,Double>(this.getComponentPosition(this.getRandomDoubleValue()), 
										   this.getComponentPosition(this.getRandomDoubleValue()));
	}
	
	public Double getComponentPosition(Double comp) {
		if(comp >= 0.6) {
			return 3.0;
		}else if(comp <= 0.4) {
			return -3.0;
		}
		return 0.0;
	}
	
	public static RandomGenerator getInstance() {
		if(instance == null) {
			instance = new RandomGenerator();
		}
		return instance;
	}
	
	public int getNextInt() {
		return random.nextInt(4) + 1;
	}
}
