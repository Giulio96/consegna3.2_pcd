package model;

import akka.actor.AbstractActor;
import akka.actor.AbstractActor.Receive;
import akka.actor.ActorRef;
import messages.GuardianStateMsg;
import messages.GuardiansReferenceMsg;
import messages.PreAlertMsg;
import messages.SensorPositionMsg;
import messages.ValueNotificationMsg;
import utilities.ActorPosition;

/*
 * Ricevono i messaggi da tutti i sensori collocati nel patch relativo
 */
public class Guardian extends AbstractActor {

	private Integer id;
	private Pair<Double,Double> myPosition;
	private Double sensorAverageValue;
	private State state;
	// Si presuppone che ogni guardiamo abbia il riferimento del proprio patch observer
	private ActorRef myPatch;
	private ActorRef view;
	
	public Guardian(final Integer id, final Pair<Double,Double> position, ActorRef patch, ActorRef view) {
		this.myPosition = position;
		this.sensorAverageValue = 0.0;
		this.state = State.QUIET;
		this.myPatch = patch;
		this.view = view;
		this.id = id;
		this.view.tell(new GuardianStateMsg(new ActorPosition(this.id, this.myPosition), this.state), this.view);
	}
	
	private void setState(State s) {
		this.state = s;
		this.view.tell(new GuardianStateMsg(new ActorPosition(this.id, this.myPosition), this.state), view);
	}

	public Receive createReceive() {
		return receiveBuilder().match(ValueNotificationMsg.class, msg -> {
			Double value = msg.getContent();
			this.analyzeNewValueFromSensor(value);
		}).build();
	}
	
	private void analyzeNewValueFromSensor(Double value) {
//		if(value > 0.5) {
//			this.state = State.QUIET;
//		}
		
		// Questa non � la formula corretta e comunque bisogna pensare ad un approccio
		this.sensorAverageValue = (this.sensorAverageValue + value)*50/100;
		
		if (this.sensorAverageValue > 0.7) {
			this.setState(State.PREALERT);
			this.myPatch.tell(new PreAlertMsg(), myPatch);
		}
	}
	
	public Pair<Double, Double> getPosition() {
		return this.myPosition;
	}
	
	public State getState() {
		return this.state;
	}
}
