package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import akka.actor.ActorRef;
import view.MapViewer;

public class Patch {

	private Pair<Integer, Integer> id;
	private List<ActorRef> guardians;
	private List<LocalDateTime> alerts;
	private State state;
	private Quadrant quadrant;
	private int countPreAlert;
	private MapViewer map;
	
	public Patch(final Pair<Integer, Integer> id, final Quadrant quadrant) {
		this.id = id;
		this.state = State.QUIET;
		this.quadrant = quadrant;
		this.countPreAlert = 0;
		this.alerts = new ArrayList<LocalDateTime>();
	}
	
	public void setGuardiansRef(final List<ActorRef> guardians) {
		this.guardians = guardians;
	}
	
	public void setState(State s) {
		this.state = s;
	}
	
	public List<ActorRef> getGuardiansInPatch(){
		return this.guardians;
	}
	
	public Pair<Integer, Integer> getId() {
		return this.id;
	}
	
	public Quadrant getQuadrant() {
		return this.quadrant;
	}
	
	public void reportPreAlert() {
		this.countPreAlert++;
		if (this.countPreAlert > this.guardians.size() * 0.5) {
			this.alerts.add(LocalDateTime.now());
			this.setState(State.ALERT);
			this.map.notifyAlert(this.id);
			
		}
	}
	
	public void reportPreAlertResolved() {
		this.countPreAlert--;
	}
	

	public void alertResolved() {
		this.alerts = new ArrayList<>();
		this.setState(State.QUIET);
	}
	
	public void setMapViewer(MapViewer m) {
		this.map = m;
	}
}
