package model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import akka.actor.AbstractActor;
import akka.actor.AbstractActor.Receive;
import akka.actor.ActorRef;
import messages.GuardiansReferenceMsg;
import messages.SensorPositionMsg;
import messages.UpdateSensorPositionMsg;
import messages.ValueNotificationMsg;
import utilities.ActorPosition;
import utilities.Constants;
import view.ViewActor;

public class Sensor extends AbstractActor {

	private Integer Id;
	private Double myValue;
	private Pair<Double, Double> myPosition;
	private List<ActorRef> myGuardians;
	private SensorMovement movement;
	private ActorRef view;
	private List<ActorRef> patchObsList;
	
	public Sensor(final Integer id, final Pair<Double,Double> position, ActorRef view, List<ActorRef> patchObsList) {
		this.Id = id;
		this.myPosition = position;
		this.myValue = RandomGenerator.getInstance().getRandomDoubleValue();
		this.movement = new SensorMovement(this.getSelf());
		this.movement.start();
		this.myGuardians = new ArrayList<>();
		this.view = view;
		this.patchObsList = patchObsList;
	}
	
	public Receive createReceive() {
		return receiveBuilder()
		  .match(GuardiansReferenceMsg.class, msg -> {
			List<ActorRef> guardians = msg.getContent();
			this.myGuardians = guardians;
		}).match(UpdateSensorPositionMsg.class,  msg -> {
			this.myPosition = new PairImpl<Double,Double>(this.myPosition.getComponentX() + msg.getContent().getComponentX(), 
																		   this.myPosition.getComponentY() + msg.getContent().getComponentY());
			this.myValue = RandomGenerator.getInstance().getRandomDoubleValue();
			this.view.tell(new SensorPositionMsg(new ActorPosition(this.Id, this.myPosition), this.myValue, getSelf()), this.view);
			if (this.myGuardians.size() == 0) {
				for (ActorRef p : this.patchObsList) {
					p.tell(new SensorPositionMsg(new ActorPosition(this.Id, this.myPosition), this.myValue, getSelf()), p);
				}
			} else {
				for (ActorRef g : this.myGuardians) {
					g.tell(new ValueNotificationMsg(this.myValue), g);
				}
			}
			
		}).build();
	}
	
	public static class SensorMovement extends Thread {
		
		ActorRef sensor;
		
		public SensorMovement(ActorRef sens) {
			this.sensor = sens;
		}
		
		public void run() {
			while(true) {
				try {
					Thread.sleep(Constants.sensorSpeedTime);
					sensor.tell(new UpdateSensorPositionMsg(RandomGenerator.getInstance().getNewRandomPosition()), sensor);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
	}
	
	
}
