package model;

public class PairImpl<X,Y> implements Pair<X,Y>{

	private X firstComponent;
	private Y secondComponent;
	
	public PairImpl(X firstC, Y secondC) {
		this.firstComponent = firstC;
		this.secondComponent = secondC;
	}
	

	@Override
	public void setComponents(Pair<X,Y> newComponents) {
		this.firstComponent = newComponents.getComponentX();
		this.secondComponent = newComponents.getComponentY();
	}

	@Override
	public boolean equals(Pair<X,Y> cPoint) {
		return (this.firstComponent == cPoint.getComponentX() && this.secondComponent == cPoint.getComponentY());
	}


	@Override
	public X getComponentX() {
		return this.firstComponent;
	}


	@Override
	public Y getComponentY() {
		return this.secondComponent;
	}
	
	public String toString() {
		return "["+this.firstComponent+" , "+this.secondComponent+"]";
	}

}
