package model;

import utilities.Constants;

public class Quadrant {

	private Pair<Double, Double> topLeft;
	private Pair<Integer, Integer> index;
	
	public Quadrant(final Pair<Integer, Integer> index) {
		this.index = index;
		
		this.topLeft = new PairImpl<Double, Double>(this.index.getComponentX()*Constants.patchSize, this.index.getComponentY()*Constants.patchSize);
	}
	
	public Boolean isThisPointInside(Pair<Double,Double> position) {
		return  this.topLeft.getComponentX() <= position.getComponentX() && 
				this.topLeft.getComponentY() <= position.getComponentY() && 
				this.topLeft.getComponentX() + Constants.patchSize >= position.getComponentX() &&
				this.topLeft.getComponentY() + Constants.patchSize >= position.getComponentY();
	}
	
	public Pair<Double, Double> getTopLeftCorner() {
		return this.topLeft;
	}
}
