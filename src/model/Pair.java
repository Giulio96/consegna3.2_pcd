package model;

public interface Pair<X,Y> {
	
	void setComponents(Pair<X,Y> newComponents);
	
	X getComponentX();

	Y getComponentY();
	
	boolean equals(Pair<X,Y> cPoint);
	
	String toString();
	
}
