package model;

import java.util.ArrayList;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import messages.GuardiansReferenceMsg;
import messages.PreAlertMsg;
import messages.ResolvedPreAlertMsg;
import messages.SensorPositionMsg;

public class PatchObserver extends AbstractActor {

	private Patch toObserve;
	
	public PatchObserver(final Patch patch) {
		this.toObserve = patch;
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder().match(SensorPositionMsg.class, msg -> {
			Pair<Double,Double> position = msg.getActorPosition().getPosition();
			if (this.toObserve.getQuadrant().isThisPointInside(position)) {
				msg.sender().tell(new GuardiansReferenceMsg(this.toObserve.getGuardiansInPatch()), msg.sender());
			}
		}).match(PreAlertMsg.class, msg -> {
			this.toObserve.reportPreAlert();
		}).match(ResolvedPreAlertMsg.class, msg -> {
			this.toObserve.reportPreAlertResolved();
		}).build();
	}
	
	
}
