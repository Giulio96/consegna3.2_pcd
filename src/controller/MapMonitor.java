package controller;

import java.util.ArrayList;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import model.Guardian;
import model.Pair;
import model.PairImpl;
import model.Patch;
import model.PatchObserver;
import model.Quadrant;
import model.RandomGenerator;
import model.Sensor;
import utilities.Constants;
import view.MapViewer;
import view.ViewActor;

public class MapMonitor {

	public static void main(String[] args) {
		List<ActorRef> guardians = new ArrayList<>();
		List<ActorRef> sensors = new ArrayList<>();
		List<ActorRef> patchObsList = new ArrayList<>();
		List<Patch> patchList = new ArrayList<>();
		
		ActorSystem system = ActorSystem.create("MySystem");
		
		for (int i = 0; i < Constants.patchInRow; i++) {
			for (int j = 0; j < Constants.patchInRow; j++) {
				Patch patch = new Patch(new PairImpl<Integer, Integer>(i, j), new Quadrant(new PairImpl<Integer, Integer>(i, j)));
				patchList.add(patch);
				ActorRef patchObs = system.actorOf(Props.create(PatchObserver.class, patch));
				patchObsList.add(patchObs);
			}
		}
		
		MapViewer map = new MapViewer(patchList);
		ActorRef view = system.actorOf(Props.create(ViewActor.class, map));
		
		for (Patch p : patchList) {
			p.setMapViewer(map);
		}
		
		for (ActorRef p : patchObsList) {
			int numGuardians = RandomGenerator.getInstance().getNextInt();
			List<ActorRef> guardiansInPatch = new ArrayList<>();
			for (int i = 0; i < numGuardians; i++) {
				ActorRef g = system.actorOf(Props.create(Guardian.class, guardians.size(), RandomGenerator.getInstance().getGuardianPosition(patchList.get(patchObsList.indexOf(p)).getId()), p, view));
				guardians.add(g);
				guardiansInPatch.add(g);
			}
			patchList.get(patchObsList.indexOf(p)).setGuardiansRef(guardiansInPatch);
		}
		
		for (int i = 0; i < Constants.StartingSensorsNum; i++) {
			Pair<Double, Double> pos = new PairImpl<>(RandomGenerator.getInstance().getRandomDoubleValue()*Constants.MapSize, RandomGenerator.getInstance().getRandomDoubleValue()*Constants.MapSize);
			ActorRef s = system.actorOf(Props.create(Sensor.class, i, pos, view, patchObsList));
			sensors.add(s);
		}
		
	}

}
