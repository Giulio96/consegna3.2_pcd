package utilities;

import model.Pair;

public class ActorPosition {
	
	private Integer id;
	private Pair<Double, Double> position;
	
	public ActorPosition(Integer id, Pair<Double, Double> position) {
		this.id = id;
		this.position = position;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	
	public Pair<Double, Double> getPosition() {
		return this.position;
	}

}
