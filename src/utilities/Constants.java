package utilities;

import java.awt.Color;

public class Constants {

	public static final double MapSize = 600.0;
	public static final int PatchNum = 9;
	public static final int patchInRow = 3;
	public static final int StartingSensorsNum = 100;
	public static final int SensorSize = 4;
	public static final int GuardianSize = 8;
	public static final Color SensorColor = Color.blue;
	public static final Color PreAlertColor = Color.orange;
	public static final Color QuietColor = Color.white;
	public static final Color AlertColor = Color.red;
	public static final Color MapColor = new Color(119, 230, 45);
	public static final int sensorSpeedTime = 100;
	public static final int viewRefreshTime = 100;
	public static final double patchSize = MapSize/patchInRow;
}
