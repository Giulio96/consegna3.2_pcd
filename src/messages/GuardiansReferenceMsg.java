package messages;

import java.util.List;

import akka.actor.ActorRef;

public class GuardiansReferenceMsg {

	private List<ActorRef> guardians;
	
	public GuardiansReferenceMsg(final List<ActorRef> guardians) {
		this.guardians = guardians;
	}
	
	public List<ActorRef> getContent(){
		return this.guardians;
	}
}
