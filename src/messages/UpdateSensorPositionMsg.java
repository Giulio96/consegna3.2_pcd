package messages;

import model.Pair;

public class UpdateSensorPositionMsg {
	private Pair<Double, Double> position;
	
	public UpdateSensorPositionMsg(final Pair<Double,Double> position) {
		this.position = position;
	}
	
	public Pair<Double, Double> getContent(){
		return this.position;
	}
}
