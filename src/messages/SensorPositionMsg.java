package messages;

import akka.actor.ActorRef;
import utilities.ActorPosition;

public class SensorPositionMsg {

	private ActorPosition actorPos;
	private Double value;
	private ActorRef sender;
	
	public SensorPositionMsg(final ActorPosition actorPos, Double value, ActorRef sender) {
		this.actorPos = actorPos;
		this.value = value;
		this.sender = sender;
	}
	
	public ActorPosition getActorPosition(){
		return this.actorPos;
	}
	
	public Double getValue() {
		return this.value;
	}
	
	public ActorRef sender() {
		return this.sender;
	}
}
