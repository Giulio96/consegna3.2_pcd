package messages;

import java.util.List;

import akka.actor.ActorRef;

public class ValueNotificationMsg {

	private Double value;

	public ValueNotificationMsg(final Double value) {
		this.value = value;
	}
	
	public Double getContent(){
		return this.value;
	}
}
