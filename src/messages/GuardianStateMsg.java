package messages;

import model.State;
import utilities.ActorPosition;

public class GuardianStateMsg {
	
	private ActorPosition actorPos;
	private State state;
	
	public GuardianStateMsg(ActorPosition actorPos, State state) {
		this.actorPos = actorPos;
		this.state = state;
	}
	
	public ActorPosition getActorPosition() {
		return this.actorPos;
	}

	public State getState() {
		return this.state;
	}
}
